import React from 'react';
import ReactDOM from 'react-dom';
import Fluxxor from 'fluxxor';

import App from './App';
import {actions} from './actions'
import {store} from './store'

const stores = {
    NewStore: new store()
};

const flux = new Fluxxor.Flux(stores, actions);
window.flux = flux;

flux.on("dispatch", function(type, payload) {
    if (console && console.log) {
        console.log("[Dispatch]", type, payload);
    }
});

ReactDOM.render(<App flux={flux}/>, document.getElementById('root'));
