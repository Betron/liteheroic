import Fluxxor from 'fluxxor'

var constants = {
    ADD_USER: "ADD_USER",
    EDIT_USER: "EDIT_USER",
    DELETE_USER: "DELETE_USER",
    DELETE_SITE: "DELETE_SITE",
    ADD_SITE: "ADD_SITE",
    EDIT_SITE: "EDIT_SITE"
};
export const store = Fluxxor.createStore({
    initialize:  function() {
        this.usersAndSites = [
            {
                name: "User1",
                sites: [
                    {
                        name: "dogs.com"
                    },
                    {
                        name: "cats.com",
                    }
                ]
            },
            {
                name: "User2",
                sites: [
                    {
                        name: "dogs2.com",
                    },
                    {
                        name: "cats2.com",
                    }
                ]
            }
        ];
        this.bindActions(
            constants.ADD_USER, this.onAddUser,
            constants.EDIT_USER, this.onEditUser,
            constants.DELETE_USER, this.onDeleteUser,
            constants.DELETE_SITE, this.onDeleteSite,
            constants.ADD_SITE, this.onAddSite,
            constants.EDIT_SITE, this.onEditSite,
        );
    },
    onAddUser : function () {
        console.log(this.usersAndSites);
        this.usersAndSites.push({name: prompt("Введите имя пользователя"),sites: {}});
        console.log(this.usersAndSites);
    },
    onEditUser : function(payload) {
        console.log(this.usersAndSites[payload.key].name);
        this.usersAndSites[payload.key].name = prompt('Введите новое название пользователя');
        console.log(this.usersAndSites[payload.key].name);
    },
    onDeleteUser : function(payload) {
        console.log(this.usersAndSites);
        this.usersAndSites = this.usersAndSites.filter((item, i) => { return i !== payload.key});
        console.log(this.usersAndSites);
    },
    onDeleteSite : function(payload) {
        console.log(this.usersAndSites);
        for(let i = 0; i< this.usersAndSites.length; i++){
            if(this.usersAndSites[i] === payload.user){
                this.usersAndSites[i].sites.splice(payload.key, 1)
                // delete this.usersAndSites[i].sites[payload.key];
            }
        }
        console.log(this.usersAndSites);
    },
    onAddSite : function(payload) {
       this.usersAndSites[payload.key].sites.push({name: prompt('Введите название нового сайта:')});
       console.log(this.usersAndSites);
    },
    onEditSite : function(payload) {
        payload.key.name = prompt('Введите новое название сайта');
        console.log(this.usersAndSites);
    },

});