import React, { Component } from 'react';

// let temp = 0;
class App extends Component {

  myStore =  this.props.flux.store("NewStore");

  render() {
    return (
      <div className="App">
        <h1>Lite Heroic</h1>
          {this.showLiteHeroic()}
        <button onClick={() => this.props.flux.actions.addUser()}>Add new user</button>
      </div>
    );
  }

  showLiteHeroic = () => {
      return(
          this.myStore.usersAndSites.map((user, key) => {
                return(
                    <div key={key} className="name">
                        {user.name}
                        <button onClick={() => this.props.flux.actions.deleteUser(key)}>Delete this user</button>
                        <button onClick={() => this.props.flux.actions.editUser(key)}>Edit this user</button>
                        {/*{temp = key}*/}
                        {user.sites.map((site, key, array) => {
                            return(
                                <div key={key}>
                                    {site.name}
                                    <button onClick={() => this.props.flux.actions.deleteSite(user, key)}>Delete this site</button>
                                    <button onClick={() => this.props.flux.actions.editSite(array[key])}>Edit this site</button>
                                </div>
                            )
                        })}
                        <button onClick={() => this.props.flux.actions.addSite(key)}>Add new site</button>
                    </div>

                )
          })
      )
  }
}

export default App;
