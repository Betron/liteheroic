export const actions = {
    addUser : function() {
        this.dispatch("ADD_USER");
    },
    editUser : function(key) {
        this.dispatch("EDIT_USER", {key: key});
    },
    deleteUser : function(key) {
       this.dispatch("DELETE_USER", {key: key});
    },
    deleteSite : function(user, key) {
        this.dispatch("DELETE_SITE", {user: user, key: key});
    },
    addSite : function(key) {
        this.dispatch("ADD_SITE", {key: key});
    },
    editSite : function(key) {
        this.dispatch("EDIT_SITE", {key: key});
    },
};